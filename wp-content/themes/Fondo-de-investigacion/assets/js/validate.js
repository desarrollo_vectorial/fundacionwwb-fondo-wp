jQuery(document).ready(function($){
    $("#form-fundacion").validate({
        rules: {
            nombres_apellidos: { required: true, minlength: 4},            
            email: { required:true, minlength: 2, email: true},
            numero_telefono: { required:true, minlength: 7, maxlength: 10, number: true },
            pais: { required: true},
            interesado_en: { required: true},
        },
        messages: {
            nombres_apellidos: "Debe introducir sus nombres y apellidos completos.",
            email : "Debe escribir un correo electrónico válido.",
            numero_telefono : "Por favor introduzca un número de celular.",
            pais: "Este campo es obligatorio.",
            interesado_en: "Este campo es obligatorio.",
        },
        submitHandler: function(form){
            // var dataString = 'nombre='+$('#comotellamas').val()+'&cedula='+$('#numcedula').val()+'...';
            var nombres_apellidos = document.getElementById('nombres_apellidos').value;
            var email = document.getElementById('email').value;
            var numero_telefono = document.getElementById('numero_telefono').value;
            var pais = document.getElementById('pais').value;
            var interesado_en = document.getElementById('interesado_en').value;

            $.ajax({
                type: "POST",
                // url: window.location.origin+"/wp-content/themes/Fondo-de-investigacion/send.php",
                url: window.location.origin+"/send.php",
                data: {nombres_apellidos:nombres_apellidos, email:email, numero_telefono:numero_telefono, pais:pais, interesado_en:interesado_en},
                success: function(data){
                    /*$('#ok').addClass('show-popup');*/
                    //$("#ok").html(data);
                    //$('#ok').addClass('show-popup');
					window.location.href = window.location.origin+"/contacto";
                    //$("#ok").show();
                    $(".spin-load").hide();
                }
            });
        }
    });


});
