    <div class="section fp-auto-height azul" id="section10">
        <?php include get_template_directory() . "/fragments/footer.php"; ?>
    </div>
</div>
<div id="infoMenu"></div>
<div id="ok"></div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/scrolloverflow.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/jquery.fullPage.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/validate.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/cookie.js"></script>

<script type="text/javascript" src="https://cdn.carbonads.com/carbon.js?zoneid=1673&serve=C6AILKT&placement=codyhouseco"></script>
<script type="text/javascript" src="https://codyhouse.co/demo/stretchy-navigation/js/main.js"></script>
<script>
    $(document).ready(function(){
        $('.icon-search').click( function(){
            $('body').toggleClass('search-active');
            $('.input-search').focus();
        });

        $('.close-search').click( function(){
            $('body').removeClass('search-active');
        });

        $('.tabs').tabs();

    });
</script>
<?php if (is_page('home')): ?>
<script>
    $(document).ready(function () {

        $('#fullpage').fullpage({
            anchors: ['firstpage', 'secondpage', 'thirdpage', 'fourthpage', 'fifthpage', 'Footer'],
            menu: '#menu',
            'navigation': true,
            'navigationPosition': 'right',
            //scrollOverflow: false,
            /*normalScrollElements: '#section2, #convocatoria',*/
            responsiveWidth: 1200
        });
        $('a.ancla').click(function (e) {
            e.preventDefault();
            var strAncla = $(this).attr('href');
            $('body,html').stop(true, true).animate({
                scrollTop: $(strAncla).offset().top
            }, 1000);

        });
    });
</script>
<?php elseif (is_page('que-es-el-fondo') || is_page('what-is-the-research-fund')): ?>
    <script>
        $(document).ready(function () {

            $('#fullpage').fullpage({
                anchors: ['firstpage', 'secondpage', 'thirdpage', 'fourthpage', 'fifthpage', 'Footer'],
                menu: '#menu',
                'navigation': true,
                'navigationPosition': 'right',
                responsiveWidth: 1200
            });
            $('a.ancla').click(function (e) {
                e.preventDefault();
                var strAncla = $(this).attr('href');
                $('body,html').stop(true, true).animate({
                    scrollTop: $(strAncla).offset().top
                }, 1000);

            });
        });
    </script>
<?php elseif (is_page('convocatoria-2022') || is_page('call-2019') || is_page('convocatoria')): ?>
    <script>
        $(document).ready(function () {

            $('#fullpage').fullpage({
                anchors: ['firstpage', 'secondpage', 'thirdpage', 'fourthpage', 'fifthpage', 'sixthpage', 'seventhpage', 'eighthpage', 'Footer'],
                menu: '#menu',
                'navigation': true,
                'navigationPosition': 'right',
                scrollOverflow: true,
                responsiveWidth: 1200
            });
            $('a.ancla').click(function (e) {
                e.preventDefault();
                var strAncla = $(this).attr('href');
                $('body,html').stop(true, true).animate({
                    scrollTop: $(strAncla).offset().top
                }, 1000);

            });
        });
    </script>
<?php elseif (is_page('convocatorias-anteriores') || is_page('previous-calls') ): ?>
    <script>
        $(document).ready(function () {

            $('#fullpage').fullpage({
                anchors: ['firstpage', 'secondpage', 'thirdpage', 'fourthpage', 'Footer'],
                menu: '#menu',
                'navigation': true,
                'navigationPosition': 'right',
                scrollOverflow: true,
                responsiveWidth: 1200
            });
            $('a.ancla').click(function (e) {
                e.preventDefault();
                var strAncla = $(this).attr('href');
                $('body,html').stop(true, true).animate({
                    scrollTop: $(strAncla).offset().top
                }, 1000);

            });
        });
    </script>
<?php elseif (is_page('proyecto') || is_page('submitting-my-project')): ?>
    <script>
        $(document).ready(function () {

            $('#fullpage').fullpage({
                anchors: ['firstpage', 'secondpage', 'Footer'],
                menu: '#menu',
                'navigation': true,
                'navigationPosition': 'right',
                scrollOverflow: true,
                responsiveWidth: 1200
            });
            $('a.ancla').click(function (e) {
                e.preventDefault();
                var strAncla = $(this).attr('href');
                $('body,html').stop(true, true).animate({
                    scrollTop: $(strAncla).offset().top
                }, 1000);

            });
        });
    </script>
    <script>

       /* Event snippet for Leads FFI conversion page
        In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. */

        function gtag_report_conversion(url) {
            var callback = function () {
                if (typeof(url) != 'undefined') {
                    window.location = url;
                }
            };
            gtag('event', 'conversion', {
                'send_to': 'AW-962176685/OAWHCO7wgYgBEK3N5soD',
                'event_callback': callback
            });
            return false;
        }
    </script>



<?php elseif (is_page('preguntas-frecuentes') || is_page('faqs')): ?>
    <script>
        $(document).ready(function () {
            $('#fullpage').fullpage({
                anchors: ['firstpage', 'secondpage', 'Footer'],
                menu: '#menu',
                'navigation': true,
                'navigationPosition': 'right',
                /*normalScrollElements: '#normalscrollvideos',*/
                scrollOverflow: true,
                responsiveWidth: 1200
            });
            $('a.ancla').click(function (e) {
                e.preventDefault();
                var strAncla = $(this).attr('href');
                $('body,html').stop(true, true).animate({
                    scrollTop: $(strAncla).offset().top
                }, 1000);

            });
        });
    </script>
<?php elseif (is_page('contacto') || is_page('contact')): ?>
    <script>
        $(document).ready(function () {

            $('#fullpage').fullpage({
                anchors: ['firstpage', 'Footer'],
                menu: '#menu',
                'navigation': true,
                'navigationPosition': 'right',
                scrollOverflow: true,
                responsiveWidth: 1200
            });
            $('a.ancla').click(function (e) {
                e.preventDefault();
                var strAncla = $(this).attr('href');
                $('body,html').stop(true, true).animate({
                    scrollTop: $(strAncla).offset().top
                }, 1000);

            });
        });
    </script>
    <script>
        $('input[type=submit]').click(function () {
            $('.spin-load').show();
        });
    </script>
<?php endif ?>
<?php wp_footer(); ?>
</body>
</html>