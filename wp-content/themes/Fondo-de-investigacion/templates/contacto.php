<?php
/* Template Name: CONTACTO */
get_header();
wp_head();
?>
<style>
    .flex.fullCenter.contentSectionBanner {
        background: url(/wp-content/uploads/2022/02/contacto-1.jpg) left;
        background-size: cover;
        text-align: center;
        color: #fff;
    }
</style>
<div id="fullpage">
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
    <?php endif; ?>


<?php
    get_footer();
?>
