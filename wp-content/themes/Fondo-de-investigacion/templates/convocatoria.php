<?php
/* Template Name: CONVOCATORIA */
get_header();
wp_head();
?>
<style>
    .flex.fullCenter.contentSectionBanner {
        background: url(/wp-content/uploads/2022/02/convocatoria1.jpg) center;
        background-size: cover;
    }
</style>
<div id="fullpage">

    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
    <?php endif; ?>


<?php
    get_footer();
?>
