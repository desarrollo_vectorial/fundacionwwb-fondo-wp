<?php
/* Template Name: SALA DE PRENSA */
get_header();
wp_head();
?>
<style>
    .anclaFoot{
        display: none;
    }
</style>
<div id="content-padding-width" class="container-fluid">
    <div class="row">
        <div class="col-md-12 background-header-internas" style="background-image: url(/wp-content/uploads/2022/02/prensa.jpg); margin-top: 0px; background-size: cover;">
            <?php echo '<h1 class="white-text" id="title-internas">' ?>
            <?php echo get_the_title(); ?>
            <?php echo '</h1>' ?>
        </div>
    </div>
    <div class="row" id="content-internas">
        <div class="col-md-12">
            <div class="row contenedor">
                <!-- <div class="col-md-9">
                    <ul id="tabs-swipe-demo" class="tabs tabs-fixed-width">
                        <li class="tab"><a href="#todos" class="active">Todos</a></li>
                        <li class="tab"><a href="#educacion">Educación financiera</a></li>
                        <li class="tab"><a href="#interes">Interés</a></li>
                        <li class="tab"><a href="#negocios">Negocios</a></li>
                        <li class="tab"><a href="#empresarios">Nuestros empresarios</a></li>
                        <li class="tab"><a href="#glosario">Glosario</a></li>
                    </ul>
                </div> -->
                <div class="col-lg-9 col-md-8  col-xs-12 col-sm-12">
                    <div id="todos" class="col-md-12" style="margin-top: 30px">
                        <!--<div id="LoaderMain"class="col-md-12">
                            <div class="progress">
                                <div class="indeterminate"></div>
                            </div>
                        </div>-->
                        <div class="content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div id="sala-de-prensa" class="col s12">
                                        <div class="row">

                                            <?php
                                            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                                            $args = array(
                                                'post_type'   => 'post',
                                                'post_status' => 'publish',
                                                'category_name' => 'sala-de-prensa,blog',
                                                'category__not_in' => array( 12, 13 ),
                                                'posts_per_page' => 30,
                                                'paged' => $paged,
                                            );

                                            $todos = new WP_Query( $args ); ?>

                                            <?php if( $todos->have_posts() ) : ?>
                                                <?php while( $todos->have_posts() ) : $todos->the_post(); ?>
                                                    <?php
                                                    $post_date = get_the_date( 'j M, Y' );
                                                    $category = get_the_category();
                                                    ?>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                                        <div class="card sticky-action">
                                                            <div class="card-image">
                                                                <?php if (has_post_thumbnail()):?>
                                                                    <img src="<?php the_post_thumbnail_url(); ?>">
                                                                <?php else: ?>
                                                                    <img src="http://via.placeholder.com/350x250" alt="">
                                                                <?php endif; ?>
                                                                <div class="date-blog">
                                                                    <?php echo $post_date ;?>
                                                                </div>
                                                                <div class="social-network-blog">
                                                                    <ul>
                                                                        <li><a target="_blank" href="http://twitter.com/home?status=<?php echo urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?>: <?php the_permalink(); ?>" title="<?php _e('Share this post on Twitter!', 'cryst4l')?>"><i class="fa fa-twitter"></i> </a></li>
                                                                        <li><a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="<?php _e('Share this post on Facebook!', 'cryst4l')?>"><i class="fa fa-facebook"></i></a></li>
                                                                        <li><a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" title="<?php _e('Share this post on Google Plus!', 'cryst4l')?>"><i class="fa fa-google-plus"></i></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="card-content" style="position: relative">
                                                                <p class="categoria-card"><?php echo $category[0]->cat_name; ?><a class="activator btn-floating waves-effect waves-light plus">
                                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>
                                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>
                                                                    </a></p>
                                                                <div>
                                                                    <a class="title-cat-card" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                                </div>
                                                            </div>

                                                            <div class="card-action">
                                                                <a href="<?php the_permalink(); ?>" class="waves-effect btn z-depth-0 grey btn-nav-principal">Leer más</a>
                                                            </div>

                                                            <div class="card-reveal">
                                                                <span class="card-title grey-text text-darken-4"><?php the_title(); ?><i class="fa fa-times right" style="line-height: 40px;"></i></span>
                                                                <p><?php the_excerpt(); ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endwhile; wp_reset_postdata(); ?>

                                                <?php
                                                if (function_exists(custom_pagination)) {
                                                    custom_pagination($todos->max_num_pages,"",$paged);
                                                }
                                                ?>
                                            <?php else:  ?>
                                                <p><?php _e( 'Disculpe aún no tenemos post en esta categoría' ); ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
<!--                    <div id="educacion" class="col-md-12" style="margin-top: 30px">-->
<!--                        <!--<div id="LoaderMain"class="col-md-12">-->
<!--                            <div class="progress">-->
<!--                                <div class="indeterminate"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="content">-->
<!--                            <div class="row">-->
<!--                            --><?php
//                            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
//
//                            $args = array(
//                                'post_type'   => 'post',
//                                'post_status' => 'publish',
//                                'posts_per_page' => 6,
//                                'category_name' => 'educacion-financiera',
//                                'paged' => $paged,
//                            );
//
//                            $educacion = new WP_Query( $args ); ?>
<!---->
<!--                            --><?php //if( $educacion->have_posts() ) : ?>
<!--                                --><?php //while( $educacion->have_posts() ) : $educacion->the_post(); ?>
<!--                                    --><?php
//                                    $post_date = get_the_date( 'j M, Y' );
//                                    $category = get_the_category();
//                                    ?>
<!--                                    <div class="col-md-4 col-xs-12">-->
<!--                                        <div class="card sticky-action">-->
<!--                                            <div class="card-image">-->
<!--                                                --><?php //if (has_post_thumbnail()):?>
<!--                                                    <img src="--><?php //the_post_thumbnail_url(); ?><!--">-->
<!--                                                --><?php //else: ?>
<!--                                                    <img src="http://via.placeholder.com/350x250" alt="">-->
<!--                                                --><?php //endif; ?>
<!--                                                <div class="date-blog">-->
<!--                                                    <p>--><?php //echo $post_date ;?><!--</p>-->
<!--                                                </div>-->
<!--                                                <div class="social-network-blog">-->
<!--                                                    <ul>-->
<!--                                                        <li><a target="_blank" href="http://twitter.com/home?status=--><?php //echo urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?><!--: --><?php //the_permalink(); ?><!--" title="--><?php //_e('Share this post on Twitter!', 'cryst4l')?><!--"><i class="fa fa-twitter"></i> </a></li>-->
<!--                                                        <li><a target="_blank" href="http://www.facebook.com/sharer.php?u=--><?php //the_permalink();?><!--&amp;t=--><?php //the_title(); ?><!--" title="--><?php //_e('Share this post on Facebook!', 'cryst4l')?><!--"><i class="fa fa-facebook"></i></a></li>-->
<!--                                                        <li><a target="_blank" href="https://plus.google.com/share?url=--><?php //the_permalink(); ?><!--" title="--><?php //_e('Share this post on Google Plus!', 'cryst4l')?><!--"><i class="fa fa-google-plus"></i></a></li>-->
<!--                                                    </ul>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class="card-content" style="position: relative">-->
<!--                                                <p class="categoria-card">--><?php //echo $category[0]->cat_name; ?><!--<a class="activator btn-floating waves-effect waves-light grey" style="position: absolute;right: 24px;bottom:80%;">-->
<!--                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>-->
<!--                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>-->
<!--                                                    </a></p>-->
<!--                                                <div>-->
<!--                                                    <a class="grey-text" href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="card-action">-->
<!--                                                <a href="--><?php //the_permalink(); ?><!--" class="waves-effect btn z-depth-0 grey btn-nav-principal">LEER MÁS</a>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="card-reveal">-->
<!--                                                <span class="card-title grey-text text-darken-4">--><?php //the_title(); ?><!--<i class="fa fa-times right" style="line-height: 40px;"></i></span>-->
<!--                                                <p>--><?php //the_excerpt(); ?><!--</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                --><?php //endwhile; wp_reset_postdata(); ?>
<!---->
<!--                                --><?php
//                                if (function_exists(custom_pagination_educacion)) {
//                                    custom_pagination_educacion($educacion->max_num_pages,"",$paged);
//                                }
//                                ?>
<!--                            --><?php //else:  ?>
<!--                                <p>--><?php //_e( 'Disculpe aún no tenemos post en esta categoría' ); ?><!--</p>-->
<!--                            --><?php //endif; ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div id="interes" class="col-md-12" style="margin-top: 30px">-->
<!--                        <!--<div id="LoaderMain"class="col-md-12">-->
<!--                            <div class="progress">-->
<!--                                <div class="indeterminate"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="content">-->
<!--                            <div class="row">-->
<!--                            --><?php
//                            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
//
//                            $args = array(
//                                'post_type'   => 'post',
//                                'post_status' => 'publish',
//                                'posts_per_page' => 6,
//                                'category_name' => 'interes',
//                                'paged' => $paged,
//                            );
//
//                            $interes = new WP_Query( $args ); ?>
<!---->
<!--                            --><?php //if( $interes->have_posts() ) : ?>
<!--                                --><?php //while( $interes->have_posts() ) : $interes->the_post(); ?>
<!--                                    --><?php
//                                    $post_date = get_the_date( 'j M, Y' );
//                                    $category = get_the_category();
//                                    ?>
<!--                                    <div class="col-md-4 col-xs-12">-->
<!--                                        <div class="card sticky-action">-->
<!--                                            <div class="card-image">-->
<!--                                                --><?php //if (has_post_thumbnail()):?>
<!--                                                    <img src="--><?php //the_post_thumbnail_url(); ?><!--">-->
<!--                                                --><?php //else: ?>
<!--                                                    <img src="http://via.placeholder.com/350x250" alt="">-->
<!--                                                --><?php //endif; ?>
<!--                                                <div class="date-blog">-->
<!--                                                    <p>--><?php //echo $post_date ;?><!--</p>-->
<!--                                                </div>-->
<!--                                                <div class="social-network-blog">-->
<!--                                                    <ul>-->
<!--                                                        <li><a target="_blank" href="http://twitter.com/home?status=--><?php //echo urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?><!--: --><?php //the_permalink(); ?><!--" title="--><?php //_e('Share this post on Twitter!', 'cryst4l')?><!--"><i class="fa fa-twitter"></i> </a></li>-->
<!--                                                        <li><a target="_blank" href="http://www.facebook.com/sharer.php?u=--><?php //the_permalink();?><!--&amp;t=--><?php //the_title(); ?><!--" title="--><?php //_e('Share this post on Facebook!', 'cryst4l')?><!--"><i class="fa fa-facebook"></i></a></li>-->
<!--                                                        <li><a target="_blank" href="https://plus.google.com/share?url=--><?php //the_permalink(); ?><!--" title="--><?php //_e('Share this post on Google Plus!', 'cryst4l')?><!--"><i class="fa fa-google-plus"></i></a></li>-->
<!--                                                    </ul>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class="card-content" style="position: relative">-->
<!--                                                <p class="categoria-card">--><?php //echo $category[0]->cat_name; ?><!--<a class="activator btn-floating waves-effect waves-light grey" style="position: absolute;right: 24px;bottom:80%;">-->
<!--                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>-->
<!--                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>-->
<!--                                                    </a></p>-->
<!--                                                <div>-->
<!--                                                    <a class="grey-text" href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="card-action">-->
<!--                                                <a href="--><?php //the_permalink(); ?><!--" class="waves-effect btn z-depth-0 grey btn-nav-principal">LEER MÁS</a>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="card-reveal">-->
<!--                                                <span class="card-title grey-text text-darken-4">--><?php //the_title(); ?><!--<i class="fa fa-times right" style="line-height: 40px;"></i></span>-->
<!--                                                <p>--><?php //the_excerpt(); ?><!--</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                --><?php //endwhile; wp_reset_postdata(); ?>
<!---->
<!--                                --><?php
//                                if (function_exists(custom_pagination_interes)) {
//                                    custom_pagination_interes($interes->max_num_pages,"",$paged);
//                                }
//                                ?>
<!--                            --><?php //else:  ?>
<!--                                <p>--><?php //_e( 'Disculpe aún no tenemos post en esta categoría' ); ?><!--</p>-->
<!--                            --><?php //endif; ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div id="negocios" class="col-md-4 col-xs-12" style="margin-top: 30px">-->
<!--                        <!--<div id="LoaderMain"class="col-md-12">-->
<!--                            <div class="progress">-->
<!--                                <div class="indeterminate"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="content">-->
<!--                            <div class="row">-->
<!--                            --><?php
//                            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
//
//                            $args = array(
//                                'post_type'   => 'post',
//                                'post_status' => 'publish',
//                                'posts_per_page' => 6,
//                                'category_name' => 'negocios',
//                                'paged' => $paged,
//                            );
//
//                            $negocios = new WP_Query( $args ); ?>
<!---->
<!--                            --><?php //if( $negocios->have_posts() ) : ?>
<!--                                --><?php //while( $negocios->have_posts() ) : $negocios->the_post(); ?>
<!--                                    --><?php
//                                    $post_date = get_the_date( 'j M, Y' );
//                                    $category = get_the_category();
//                                    ?>
<!--                                    <div class="col-md-4 col-xs-12">-->
<!--                                        <div class="card sticky-action">-->
<!--                                            <div class="card-image">-->
<!--                                                --><?php //if (has_post_thumbnail()):?>
<!--                                                    <img src="--><?php //the_post_thumbnail_url(); ?><!--">-->
<!--                                                --><?php //else: ?>
<!--                                                    <img src="http://via.placeholder.com/350x250" alt="">-->
<!--                                                --><?php //endif; ?>
<!--                                                <div class="date-blog">-->
<!--                                                    <p>--><?php //echo $post_date ;?><!--</p>-->
<!--                                                </div>-->
<!--                                                <div class="social-network-blog">-->
<!--                                                    <ul>-->
<!--                                                        <li><a target="_blank" href="http://twitter.com/home?status=--><?php //echo urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?><!--: --><?php //the_permalink(); ?><!--" title="--><?php //_e('Share this post on Twitter!', 'cryst4l')?><!--"><i class="fa fa-twitter"></i> </a></li>-->
<!--                                                        <li><a target="_blank" href="http://www.facebook.com/sharer.php?u=--><?php //the_permalink();?><!--&amp;t=--><?php //the_title(); ?><!--" title="--><?php //_e('Share this post on Facebook!', 'cryst4l')?><!--"><i class="fa fa-facebook"></i></a></li>-->
<!--                                                        <li><a target="_blank" href="https://plus.google.com/share?url=--><?php //the_permalink(); ?><!--" title="--><?php //_e('Share this post on Google Plus!', 'cryst4l')?><!--"><i class="fa fa-google-plus"></i></a></li>-->
<!--                                                    </ul>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class="card-content" style="position: relative">-->
<!--                                                <p class="categoria-card">--><?php //echo $category[0]->cat_name; ?><!--<a class="activator btn-floating waves-effect waves-light grey" style="position: absolute;right: 24px;bottom:80%;">-->
<!--                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>-->
<!--                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>-->
<!--                                                    </a></p>-->
<!--                                                <div>-->
<!--                                                    <a class="grey-text" href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="card-action">-->
<!--                                                <a href="--><?php //the_permalink(); ?><!--" class="waves-effect btn z-depth-0 grey btn-nav-principal">LEER MÁS</a>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="card-reveal">-->
<!--                                                <span class="card-title grey-text text-darken-4">--><?php //the_title(); ?><!--<i class="fa fa-times right" style="line-height: 40px;"></i></span>-->
<!--                                                <p>--><?php //the_excerpt(); ?><!--</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                --><?php //endwhile; wp_reset_postdata(); ?>
<!---->
<!--                                --><?php
//                                if (function_exists(custom_pagination_negocios)) {
//                                    custom_pagination_negocios($negocios->max_num_pages,"",$paged);
//                                }
//                                ?>
<!--                            --><?php //else:  ?>
<!--                                <p>--><?php //_e( 'Disculpe aún no tenemos post en esta categoría - Negocios' ); ?><!--</p>-->
<!--                            --><?php //endif; ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div id="empresarios" class="col-md-12" style="margin-top: 30px">-->
<!--                        <!--<div id="LoaderMain"class="col-md-12">-->
<!--                            <div class="progress">-->
<!--                                <div class="indeterminate"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="content">-->
<!--                            <div class="row">-->
<!--                            --><?php
//                            $empresarios = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
//
//                            $args = array(
//                                'post_type'   => 'post',
//                                'post_status' => 'publish',
//                                'posts_per_page' => 6,
//                                    'category_name' => 'nuestros-empresarios',
//                                'paged' => $paged,
//                            );
//
//                            $empresarios = new WP_Query( $args ); ?>
<!---->
<!--                            --><?php //if( $empresarios->have_posts() ) : ?>
<!--                                --><?php //while( $empresarios->have_posts() ) : $empresarios->the_post(); ?>
<!--                                    --><?php
//                                    $post_date = get_the_date( 'j M, Y' );
//                                    $category = get_the_category();
//                                    ?>
<!--                                    <div class="col-md-4 col-xs-12">-->
<!--                                        <div class="card sticky-action">-->
<!--                                            <div class="card-image">-->
<!--                                                --><?php //if (has_post_thumbnail()):?>
<!--                                                    <img src="--><?php //the_post_thumbnail_url(); ?><!--">-->
<!--                                                --><?php //else: ?>
<!--                                                    <img src="http://via.placeholder.com/350x250" alt="">-->
<!--                                                --><?php //endif; ?>
<!--                                                <div class="date-blog">-->
<!--                                                    <p>--><?php //echo $post_date ;?><!--</p>-->
<!--                                                </div>-->
<!--                                                <div class="social-network-blog">-->
<!--                                                    <ul>-->
<!--                                                        <li><a target="_blank" href="http://twitter.com/home?status=--><?php //echo urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?><!--: --><?php //the_permalink(); ?><!--" title="--><?php //_e('Share this post on Twitter!', 'cryst4l')?><!--"><i class="fa fa-twitter"></i> </a></li>-->
<!--                                                        <li><a target="_blank" href="http://www.facebook.com/sharer.php?u=--><?php //the_permalink();?><!--&amp;t=--><?php //the_title(); ?><!--" title="--><?php //_e('Share this post on Facebook!', 'cryst4l')?><!--"><i class="fa fa-facebook"></i></a></li>-->
<!--                                                        <li><a target="_blank" href="https://plus.google.com/share?url=--><?php //the_permalink(); ?><!--" title="--><?php //_e('Share this post on Google Plus!', 'cryst4l')?><!--"><i class="fa fa-google-plus"></i></a></li>-->
<!--                                                    </ul>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class="card-content" style="position: relative">-->
<!--                                                <p class="categoria-card">--><?php //echo $category[0]->cat_name; ?><!--<a class="activator btn-floating waves-effect waves-light grey" style="position: absolute;right: 24px;bottom:80%;">-->
<!--                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>-->
<!--                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>-->
<!--                                                    </a></p>-->
<!--                                                <div>-->
<!--                                                    <a class="grey-text" href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="card-action">-->
<!--                                                <a href="--><?php //the_permalink(); ?><!--" class="waves-effect btn z-depth-0 grey btn-nav-principal">LEER MÁS</a>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="card-reveal">-->
<!--                                                <span class="card-title grey-text text-darken-4">--><?php //the_title(); ?><!--<i class="fa fa-times right" style="line-height: 40px;"></i></span>-->
<!--                                                <p>--><?php //the_excerpt(); ?><!--</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                --><?php //endwhile; wp_reset_postdata(); ?>
<!---->
<!--                                --><?php
//                                if (function_exists(custom_pagination_empresarios)) {
//                                    custom_pagination_empresarios($empresarios->max_num_pages,"",$paged);
//                                }
//                                ?>
<!--                            --><?php //else:  ?>
<!--                                <p>--><?php //_e( 'Disculpe aún no tenemos post en esta categoría - Empresarios' ); ?><!--</p>-->
<!--                            --><?php //endif; ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div id="glosario" class="col-md-12" style="margin-top: 30px">-->
<!--                        <!--<div id="LoaderMain"class="col-md-12">-->
<!--                            <div class="progress">-->
<!--                                <div class="indeterminate"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="content">-->
<!--                            <div class="row">-->
<!--                            --><?php
//                            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
//
//                            $args = array(
//                                'post_type'   => 'post',
//                                'post_status' => 'publish',
//                                'posts_per_page' => 6,
//                                'category_name' => 'glosario',
//                                'paged' => $paged,
//                            );
//
//                            $glosario = new WP_Query( $args ); ?>
<!---->
<!--                            --><?php //if( $glosario->have_posts() ) : ?>
<!--                                --><?php //while( $glosario->have_posts() ) : $glosario->the_post(); ?>
<!--                                    --><?php
//                                    $post_date = get_the_date( 'j M, Y' );
//                                    $category = get_the_category();
//                                    ?>
<!--                                    <div class="col-md-4 col-xs-12">-->
<!--                                        <div class="card sticky-action">-->
<!--                                            <div class="card-image">-->
<!--                                                <img src="--><?php //bloginfo('template_url') ?><!--/assets/images/shopping-cart.jpg">-->
<!--                                                <div class="date-blog">-->
<!--                                                    <p>--><?php //echo $post_date ;?><!--</p>-->
<!--                                                </div>-->
<!--                                                <div class="social-network-blog">-->
<!--                                                    <ul>-->
<!--                                                        <li><a target="_blank" href="http://twitter.com/home?status=--><?php //echo urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?><!--: --><?php //the_permalink(); ?><!--" title="--><?php //_e('Share this post on Twitter!', 'cryst4l')?><!--"><i class="ion-social-twitter"></i> </a></li>-->
<!--                                                        <li><a target="_blank" href="http://www.facebook.com/sharer.php?u=--><?php //the_permalink();?><!--&amp;t=--><?php //the_title(); ?><!--" title="--><?php //_e('Share this post on Facebook!', 'cryst4l')?><!--"><i class="ion-social-facebook"></i></a></li>-->
<!--                                                        <li><a target="_blank" href="https://plus.google.com/share?url=--><?php //the_permalink(); ?><!--" title="--><?php //_e('Share this post on Google Plus!', 'cryst4l')?><!--"><i class="ion-social-googleplus"></i></a></li>-->
<!--                                                    </ul>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class="card-content" style="position: relative">-->
<!--                                                <p class="categoria-card">--><?php //echo $category[0]->cat_name; ?><!--<a class="activator btn-floating waves-effect waves-light grey" style="position: absolute;right: 24px;bottom:80%;">-->
<!--                                                        <i class="fa fa-plus" style="line-height: 40px;"></i>-->
<!--                                                    </a></p>-->
<!--                                                <div>-->
<!--                                                    <a class="grey-text" href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="card-action">-->
<!--                                                <a href="--><?php //the_permalink(); ?><!--" class="waves-effect btn z-depth-0 grey btn-nav-principal">LEER MÁS</a>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="card-reveal">-->
<!--                                                <span class="card-title grey-text text-darken-4">--><?php //the_title(); ?><!--<i class="fa fa-times right" style="line-height: 40px;"></i></span>-->
<!--                                                <p>--><?php //the_excerpt(); ?><!--</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                --><?php //endwhile; wp_reset_postdata(); ?>
<!---->
<!--                                --><?php
//                                if (function_exists(custom_pagination_2)) {
//                                    custom_pagination_2($clasificados->max_num_pages,"",$paged);
//                                }
//                                ?>
<!--                            --><?php //else:  ?>
<!--                                <p>--><?php //_e( 'Disculpe aún no tenemos Glosario' ); ?><!--</p>-->
<!--                            --><?php //endif; ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                    <?php
                    $query = new WP_Query( array(
                            'post_type'     => 'post', //your post type
                            'posts_per_page' => 3,
                            'category__not_in' => array( 12, 13 ),
                            'meta_key'      => 'views', //the metakey previously defined
                            'orderby'       => 'meta_value_num',
                            'order'         => 'DESC'
                        )
                    );
                    ?>
                    <?php if($query->have_posts() ) : ?>
                        <div class="row" id="mas-vistos">
                            <ul style="padding: 0">

                                <li>
                                    <p class="title-mas-vistos"><strong>Más vistos</strong></p>
                                </li>
                                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                    <?php
                                    $post_date = get_the_date( 'j M, Y' );
                                    $category = get_the_category();
                                    //$idObj = get_category_by_slug();
                                    ?>
                                    <li>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-mas-vistos">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 mas-vistos-img">
                                        <?php if (has_post_thumbnail()):?>
                                            <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>"></a>
                                        <?php else: ?>
                                            <a href="<?php the_permalink(); ?>"><img src="http://via.placeholder.com/350x250" alt="" class="img-responsive"></a>
                                        <?php endif; ?>

                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" s>
                                                    <p class="color-verde-text text-darken-4 mas-vistos-cat"><?php
                                                            echo $category[0]->cat_name;
                                                         ?></p>
                                                    <a href="<?php the_permalink(); ?>" class="mas-vistos-title"><?php the_title(); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php endwhile; wp_reset_postdata(); ?>
                            </ul>
                        </div>
                    <?php else:  ?>
                        <p><?php _e( 'Disculpe aún no se han visto posts' ); ?></p>
                    <?php endif?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    get_footer();
?>
