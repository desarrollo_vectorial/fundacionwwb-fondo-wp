<?php
/* Template Name: HOME */
get_header();
wp_head();
?>
    <div id="fullpage">

    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
    <?php endif; ?>

<?php
get_footer();
?>