<?php
get_header();
wp_head();
?>
<style>
    .anclaFoot{
        display: none;
    }
</style>
<div id="content-padding-width" class="container-fluid">
    <div class="row">
        <div class="col-md-12 background-header-internas" style="background-image: url(<?php bloginfo('template_url') ?>/assets/images/tercer-convocatoria/ffi1.jpg">
            <div class="col-md-12 search-content-page">
                <?php
                $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
                $url_en = $uriSegments[1];

                $text1_es = 'Resultados de busqueda para:';
                $text1_en = 'Search results about:';

                $text2_es = 'No se encontraron resultados';
                $text2_en = "Not results found";

                ?>
                <form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>"  role="search">
                    <input name="s" class="search-box form-control" type="search" placeholder="<?php if( $url_en == 'en'){ echo 'Search';} else{ echo 'Buscar';}?>" autocomplete="off">
                    <input  value="<?php if( $url_en == 'en'){ echo 'Search';} else{ echo 'Buscar';}?>" type="submit" class="button--amarillo">
                </form>
            </div>
        </div>
    </div>
    <div class="row" id="content-internas">
        <div class="col-md-12">
            <div class="row contenedor contenedor-busquedas">
                <div class="col-md-12">
                    <?php
                    if($url_en == 'en'){
                        $text_search = $text1_en;
                        $text_search2 = $text2_en;
                    }else{
                        $text_search = $text1_es;
                        $text_search2 = $text2_es;
                    }
                    ?>
                    <?php if(have_posts()) : ?>
                        <div class="row">
                        <h2 class="texto-busqueda"><?php printf( __( $text_search . ' <span>%s</span>'), get_search_query() ); ?></h2>
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php if(!is_page()): ?>
                                <div class="col-md-12 results item-search">
                                    <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                                    <p><?php the_excerpt(); ?></p>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                        </div>
                    <?php else: ?>
                        <h2 class="text-center"><?php  echo $text_search2?></h2>
                    <?php endif; ?>
                    <?php echo paginate_links( $args ); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
wp_footer();
get_footer();
?>
