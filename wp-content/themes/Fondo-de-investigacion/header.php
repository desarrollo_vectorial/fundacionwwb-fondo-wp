<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?> <?php bloginfo('description'); ?></title>
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url') ?>/assets/images/fondo-wwb.png"><!--Favicon-->
    <!-- AGREGA TUS ESTILOS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"/>
    <link href="<?php bloginfo('template_url') ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php bloginfo('template_url') ?>/assets/css/main.css?v=2024.09.2701" rel="stylesheet" type="text/css" />
    <link href="<?php bloginfo('template_url') ?>/assets/css/jqueryFullpage.min.css" rel="stylesheet" type="text/css" media="screen,projection" />
    <link href="<?php bloginfo('template_url') ?>/assets/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css" />
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://kit.fontawesome.com/d886c858d5.css" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/d886c858d5.js" crossorigin="anonymous"></script>
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <!-- AGREGA TUS SCRIPTS -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script> 

	<script>
            jQuery(document).ready(function($) {
                          "use strict";
                          //  TESTIMONIALS CAROUSEL HOOK
                          $('#customers-testimonials').owlCarousel({
                              loop: true,
                              center: true,
                              margin:-38,                   
                              dots:true,
                              nav: true,    
                              navText: ["<img src='https://www.fundacionwwbcolombia.org/wp-content/uploads/2023/03/arrow-left.svg'>","<img src='https://www.fundacionwwbcolombia.org/wp-content/uploads/2023/03/arrow-right.svg'>"],                  
                              autoplay:true,
                              autoplayTimeout: 3000,
                              autoplayHoverPause: true,
                              smartSpeed: 450,
                              responsive: {
                                0: {
                                  items: 1
                                },
                                768: {
                                  items: 2
                                },
                                1170: {
                                  items: 3
                                }
                              }
                          });
                      });
        </script>
	
	
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <?php
    wp_enqueue_script('jquery');
    wp_head();
    ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-34840310-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-34840310-3');
    </script>

    <!-- Global site tag (gtag.js) - Google Ads: 962176685 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-962176685"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-962176685');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '129614611015017');
        fbq('track', 'PageView');
    </script>

    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=129614611015017&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body <?php body_class(); ?>>
<div class="message-cookies">
    <div class="row">
        <div class="col-md-10">
            <p>Utilizamos cookies de terceros para recopilar información estrictamente estadística y mejorar la experiencia de navegación en nuestro sitio web. Si continúa navegando, consideramos que acepta su uso. Puede cambiar la configuración u obtener <a href="https://www.fundacionwwbcolombia.org/wp-content/uploads/2018/09/PL-03-Politicas-Web.pdf" target="_blank">más información.</a></p>
        </div>
        <div class="col-md-2">
            <?php echo do_shortcode('[contact-form-7 id="250" title="Cookies"]'); //113 ?>
        </div>
    </div>
</div>

<?php include get_template_directory() . "/fragments/nav.php"; ?>
