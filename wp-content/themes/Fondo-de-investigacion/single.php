<?php

get_header();
wp_head();

?>
    <style>
        .anclaFoot{
            display: none;
        }
    </style>
<div id="content-padding-width" class="container-fluid">
    <div class="row">

        <div class="col-md-12 background-header-internas" style="background-image: url(/wp-content/uploads/2021/02/enterate.jpg); margin-top: 0px; background-size: cover;">
        <?php if ( $pages ) : ?>
            <?php foreach ($pages as $post ) : setup_postdata( $post );?>
                <?php the_post_thumbnail_url(); ?>
            <?php endforeach; wp_reset_postdata(); ?>
        <?php endif; ?>)">
            <?php echo '<h1 class="white-text" id="title-internas">' ?>
            <?php echo get_the_title(17); ?>
            <?php echo '</h1>' ?>
        </div>

    </div>

    <div class="row" id="content-internas">
        <div class="col-md-12">
            <div class="row contenedor" style="margin-top: 40px;">
                <div class="col-md-12" >
                    <a class="atras-btn" href="/sala-de-prensa" ><i class="fa fa-angle-left"></i>  Atras</a>
                </div>
                <div class="col-lg-9 col-md-8  col-xs-12 col-sm-12">
                    <div id="todos" class="col-md-12" style="margin-top: 30px">

                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php
                            $post_date = get_the_date( 'j M, Y' );
                            $category = get_the_category();
                            ?>
                            <div class="detalle-noticia">

                                <div class="imagen-noticia">
                                    <p class="date-noticia"><?php echo $post_date ;?></p>
                                    <?php if (has_post_thumbnail()):?>
                                        <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>"></a>
                                    <?php else: ?>
                                       <img src="http://via.placeholder.com/350x250" alt="" class="img-responsive"></a>
                                    <?php endif; ?>
                                </div>

                                <div class="contenido-noticia">
                                    <p class="titulo_post"><?php the_title(); ?></p>
                                    <p class="color-verde-text"><?php echo $category[0]->cat_name; ?></p>
                                    <p class="detalle-contenido-noticia"><?php the_content(); ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                    <?php
                    $query = new WP_Query( array(
                            'post_type'     => 'post', //your post type
                            'posts_per_page' => 3,
                            'category__not_in' => array( 12, 13 ),
                            'meta_key'      => 'views', //the metakey previously defined
                            'orderby'       => 'meta_value_num',
                            'order'         => 'DESC'
                        )
                    );
                    ?>
                    <?php if($query->have_posts() ) : ?>
                        <div class="row" id="mas-vistos">
                            <ul style="padding: 0">
                                <li>
                                    <p class="title-mas-vistos"><strong>Más vistos</strong></p>
                                </li>
                                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                    <?php
                                    $post_date = get_the_date( 'j M, Y' );
                                    $category = get_the_category();
                                    ?>
                                    <li>
                                        <div class="col-md-12 col-mas-vistos">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6  mas-vistos-img">
                                                    <?php if (has_post_thumbnail()):?>
                                                        <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>"></a>
                                                    <?php else: ?>
                                                        <a href="<?php the_permalink(); ?>"><img src="http://via.placeholder.com/350x250" alt="" class="img-responsive"></a>
                                                    <?php endif; ?>

                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                                    <p class="color-verde-text text-darken-4 mas-vistos-cat"><?php echo $category[0]->cat_name; ?></p>
                                                    <a href="<?php the_permalink(); ?>" class="mas-vistos-title"><?php the_title(); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php endwhile; wp_reset_postdata(); ?>
                            </ul>
                        </div>
                    <?php else:  ?>
                        <p><?php _e( 'Disculpe aún no se han visto posts' ); ?></p>
                    <?php endif?>
                </div>
            </div>
        </div>
    </div>

</div>







<?php
    get_footer();
?>