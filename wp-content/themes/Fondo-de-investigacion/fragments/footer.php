<footer>
    <?php
    $logo_footer = get_field('ot_footer_logo', 'options');
    $iconos_redes = get_field('ot_social_network', 'options');
    ?>
    <div class="flex contentFooter">
        <div class="col2">
           
            <div class="flex menuFooter">
                <div class="col1">
                    <?php
                    wp_nav_menu(array(
                        'container'  => false,
                        'items_wrap' => '<ul>%3$s</ul>',
                        'theme_location' => 'footer-menu-1' ));
                    ?>
                </div>
                <div class="col1">
                    <?php
                    wp_nav_menu(array(
                        'container'  => false,
                        'items_wrap' => '<ul>%3$s</ul>',
                        'theme_location' => 'footer-menu-2' ));
                    ?>
                    <!--
                    <ul>
                        <li>
                            <a href="/proyecto/#firstPage">
                                Postular mi proyecto
                            </a>
                        </li>
                        <li>
                            <a href="/preguntas/#3rdPage">
                                Financiamiento
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.fundacionwwbcolombia.org/wp-content/uploads/2018/08/003-TERMINOS-DE-REFERENCIA-2018.pdf">
                                Términos y condiciones
                            </a>
                        </li>
                    </ul>-->
                </div>
                <div class="col1">
                    <?php
                    wp_nav_menu(array(
                        'container'  => false,
                        'items_wrap' => '<ul>%3$s</ul>',
                        'theme_location' => 'footer-menu-3' ));
                    ?>
                </div>
            </div>
        </div>
        <div class="col1">
            <figure class="cnt_logo">
                <a class="footer--logo" href="index.php"><img src="<?php echo $logo_footer['url'] ?>"></a>
            </figure>
            <div class="cnt_icons">
                <?php foreach( $iconos_redes as $icon ) : ?>
                    <?php if( $icon['enlace'] ) : ?>
                        <a href="<?php echo $icon['enlace'] ?>"><i class="<?php echo $icon['font_awesome_class'] ?>"></i></a>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!--<div class="vectorial azulDestacado">
        <a href="http://vectorial.co/" target="_blank"><img src="<?php bloginfo('template_url') ?>/assets/images/vectorial.png"></a>
    </div>-->
    <a class="anclaFoot" data-menuanchor="firstPage" href="#firstpage">
        <i class="fa fa-angle-up flechaArrow" aria-hidden="true"></i>
    </a>
</footer>