<header>
    <nav class="cd-stretchy-nav">
        <a class="cd-nav-trigger" href="#0">
            Menu
            <span aria-hidden="true"></span>
        </a>
        <?php
        wp_nav_menu(array(
            'container'  => false,
            'items_wrap' => '<ul>%3$s</ul>',
            'link_before'    => '<span>',
            'link_after'     => '</span>',
            'theme_location' => 'header-menu' ));
        ?> <span aria-hidden="true" class="stretchy-nav-bg"></span>
        <ul class="control">
            <li class="icon-search">
                <a href="#nogo"><i class="fa fa-search"></i> Buscar</a>
            </li>
        </ul>
    </nav>
</header>
<?php
$uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$url_en = $uriSegments[1];
$logo_header = get_field('ot_header_logo', 'options');
?>
<div class="search-content">
    <div class="close-search">
        <i class="fa fa-times-circle"></i>
    </div>
    <form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>"  role="search" >
        <input name="s" class="search-box form-control" type="search" placeholder="<?php if( $url_en == 'en'){ echo 'Search';} else{ echo 'Buscar';}?>" autocomplete="off">
        <button type="submit"><i class="fa fa-search"></i></button>
    </form>
</div>

<div class="flex fullCenter menuContenedor">
    <a href="/"><img src="<?php echo $logo_header['url'] ?>" alt="fondo fundación WWWB Colombia" style="width: 250px"></a>
    <div class="flex menuItems">

        <?php
        wp_nav_menu(array(
            'container' => false,
            'items_wrap' => '<ul class="flex menu">%3$s</ul>',
            'theme_location' => 'header-menu' ));
        ?>
        <div class="flex">
            <ul class="control flex menu">
                <li class="icon-search">
                    <a href="#nogo"><i class="fa fa-search"></i></a>
                </li>
            </ul>
        </div>
        <div class="flex lang">
            <?php do_action('wpml_add_language_selector'); ?>
        </div>
        <div class="flex fullCenter redesSociales">
            <a href="https://www.facebook.com/FundacionWWBCol" target="_blank">
                <i class="fa fa-facebook" aria-hidden="true"></i>
            </a>
            <a href="http://twitter.com/FundacionWWBCol" target="_blank">
                <i class="fa fa-twitter" aria-hidden="true"></i>
            </a>
            <a href="https://www.youtube.com/user/FundacionWWBColombia/" target="_blank">
                <i class="fa fa-youtube-play" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>