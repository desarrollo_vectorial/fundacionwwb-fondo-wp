<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');


    $conexion = @mysql_connect ('dbproduccion1.cartbphlockr.us-west-2.rds.amazonaws.com', 'fundacion_wwb', 'fuN$d4c1$0n$w3B');
    mysql_select_db ('FundacionWWBFondo', $conexion);
    mysql_query('SET NAMES utf8');
    mysql_query('SET CHARACTER SET utf8');
    $sql = "SELECT * FROM fondo_contact_form";
    $resultado = mysql_query ($sql, $conexion) or die (mysql_error ());
    $registros = mysql_num_rows ($resultado);

    

    if ($registros > 0) {
        /** Include PHPExcel */
        require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Vectorial Agencia Digital")
            ->setLastModifiedBy("Vectorial Agencia Digital")
            ->setTitle("FFI 2018")
            ->setSubject("FFI 2018")
            ->setDescription("Test developer Jonathan Gallego Londoño")
            ->setKeywords("FFI")
            ->setCategory("Registers");


        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:E1')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('263f95');

        $objPHPExcel->getActiveSheet()->getStyle("A1:E1")->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle("A:E")->getFont()->setSize(16);


        for($col = 'A'; $col !== 'E'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        //set up the style in an array
        $style = array('font' => array('size' => 18,'bold' => true,'color' => array('rgb' => 'ffffff')));

        //apply the style on column A row 1 to Column B row 1
        $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($style);

        $i = 2;
        while ($registro = mysql_fetch_object ($resultado)) {

            $objPHPExcel->setActiveSheetIndex(0)
                ->SetCellValue('A1', 'Nombres y Apellidos')
                ->SetCellValue('B1', 'E-mail')
                ->SetCellValue('C1', 'Número teléfono')
                ->SetCellValue('D1', 'País')
                ->SetCellValue('E1', 'Fecha')
                ->setCellValue('A'.$i, $registro->nombres_apellidos)
                ->setCellValue('B'.$i, $registro->email)
                ->setCellValue('C'.$i, $registro->numero_telefono)
                ->setCellValue('D'.$i, $registro->pais)
                ->setCellValue('E'.$i, $registro->fecha);
            $i++;
        }
    }

    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Base de datos');


    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);

    $fechadescarga = date("Y/F/d H:i:s");

    /*header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="registros-exponegocios-'.$fechadescarga.'.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    exit;*/

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
/*header('Content-Disposition: attachment;filename="01simple.xlsx"');*/
header('Content-Disposition: attachment;filename="FFI-'.$fechadescarga.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;