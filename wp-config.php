<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */


define('HTTP_HOST', $_SERVER["HTTP_HOST"]);
define('DEV_1', 'nombre.dominio');
define('DEV_2', 'fondo.minisite.local');
define('DEV_3', 'nombre.dominio');
define('DEV_DESARROLLO', 'ffi-portal.vectorial.co');
define('PROD_DOMAIN', 'fondoparalainvestigacion.fundacionwwbcolombia.org');
define('DEV_LOCAL_VIEW', 'nombre.dominio');


// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //

switch (HTTP_HOST) {
    case DEV_1:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://nombre.develop/');
        define('WP_SITEURL','http://nombre.develop/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', '');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', '');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case DEV_2:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://fondo.minisite.local');
        define('WP_SITEURL','http://fondo.minisite.local');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'dev_fondo_minisite');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case DEV_3:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://nombre.develop/');
        define('WP_SITEURL','http://nombre.develop/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', '');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case DEV_DESARROLLO:
        /*SERVIDOR DE VECTORIAL*/
        define('WP_HOME','http://ffi-portal.vectorial.co/');
        define('WP_SITEURL','http://ffi-portal.vectorial.co/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'fundacionwwbFondo_portal');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'vectorial$_$');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '127.0.0.1');
        break;

    case DEV_LOCAL_VIEW:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','');
        define('WP_SITEURL','');

        define('DB_NAME', '');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case PROD_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','https://fondoparalainvestigacion.fundacionwwbcolombia.org');
        define('WP_SITEURL','https://fondoparalainvestigacion.fundacionwwbcolombia.org');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'FundacionWWBFondo');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'fundacion_wwb');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'fuN$d4c1$0n$w3B');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'dbproduccion1.cartbphlockr.us-west-2.rds.amazonaws.com');
        break;

    default:
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'FundacionWWBFondo');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'fundacion_wwb');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'fuN$d4c1$0n$w3B');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'dbproduccion1.cartbphlockr.us-west-2.rds.amazonaws.com');
        break;
}
/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');


/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '<B[rH:NX.B(K;Y*I5R>/#(:.9k#Y)rCp>Ub58EvY4B_)d5CPMXObDLq.YY-]?5EC');
define('SECURE_AUTH_KEY', '3)tVx6~>BXe4_0h-}LN>~uP;(`3KfJN)cueLD/&q(&L@)A9Ik|C~Ztav^NOEumgh');
define('LOGGED_IN_KEY', 'E8I*$<32=pY@+b(HLO^<3ZHAC/dY:<:0:=$d|V;/B.hRB9AcIG)@^xfas)DOg-5E');
define('NONCE_KEY', '0<0,/?<dz|9=%0g^?C8?Nr~kMLwKz8_@oO4,02d`RP/u+*yBlVl#T&8sdYm]^]Dx');
define('AUTH_SALT', 'lhM#<X5.h1BSK)qB9P>6YW`#3RF{B6nZ#@T@D]HN~zFj23CXI3BbCk8vs=#Il[Tp');
define('SECURE_AUTH_SALT', ',UA)kJ+<8NR,A`7&5(B{ e~;l}cTlMv66Lib$?i(wNJ)xj$o:eC&0=`#Gd9y26Ue');
define('LOGGED_IN_SALT', '1Q)xf4-M&%poabo2Kz*8tnI)bZZ~7hdIYF3I%%^pR8unwBI&As^8q]AGdCNhM{,A');
define('NONCE_SALT', '-_.^m6<[2xhOxnT;Fq$X|~PwwE[;.P`(mR<sR.E)Hp! >=ZP!UetMSBQ> 3>Y Fr');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'fondowp_';

/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);
define('WP_TEMP_DIR', dirname(__FILE__) . '/wp-content/temp/');
/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

